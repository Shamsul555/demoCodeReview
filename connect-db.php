<?php

// server info
$server = '127.0.1.1';
$user = 'root';
$pass = '';
$db = 'db_data';

// connect to the database
$mysqli = new mysqli($server, $user, $pass, $db);

// show errors (remove this line if on a live site)
mysqli_report(MYSQLI_REPORT_ERROR);

?>
